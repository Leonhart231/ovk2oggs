# ovk2oggs
ovk2oggs version 1.0.0

## About
This is a Python script for converting an OVK file to several OGG files. OVK files are used in RealLive and SiglusEngine video games such as Clannad to store dialogue.

## Installation
ovk2oggs is a Python script, so no installation is necessary. All you need is a copy of Python installed. Windows and Mac users can download it from the [Python website](https://www.python.org/) (make sure to add it to your PATH). Linux and BSD users may already have it installed, or you can install it through your distribution’s package system.

> Non-Windows users: You have the ability to install ovk2oggs as an executable program. Simply move it to a folder in your PATH and make sure it is marked as executable. You can do this by running `chmod +x ovk2oggs`

## Examples
The examples below assume ovk2oggs was installed as an executable. If this was not done, either because you are on Windows or chose not to, you will need to make some changes to them:

> Windows users: Replace `ovk2oggs` with `py C:\path\to\ovk2oggs`.

> Non-Windows users: Replace `ovk2oggs` with `python3 /path/to/ovk2oggs`.

Note that this will output *a lot* of OGG files, I have seen over 3000 for some OVKs. If you output all of the OGG files for a game into a single directory, you may run in to file system limits on your computer.

The examples below convert input.ovk to OGG files. You can use any combination of input and output.

### Example 1
You can use arguments to specify the input file and output directory as follows. This will create files of the form `outdir/input-1.ogg`, `outdir/input-2.ogg`, etc.

```
ovk2oggs -i input.ovk outdir
```

### Example 2
> Windows users: This will not work on PowerShell since it does not support standard input redirection; use the Command Prompt instead.

You can use standard input and the current directory for output as follows. This will create files of the form `1.ogg`, `2.ogg`, etc.

```
ovk2oggs < input.ovk
```

## Confirmed working
The following games’ OVK files have been confirmed to properly convert with this script. If you have the chance to test others, please let me know so I can add them to the list. If you find that a game’s OVK files do not properly convert, please add an issue or email me at renters-wounds-0f at icloud.com with the information.
- Clannad (Steam version)
